FROM openjdk:11-jdk
ARG JAR_FILE
COPY /target/${JAR_FILE} spring-docker-spotify-0.0.1-SNAPSHOT.jar
ENTRYPOINT java -jar spring-docker-spotify-0.0.1-SNAPSHOT.jar